#pragma once

#include <abonement.h>

#include <QHash>
#include <QObject>

class Database;

class Abonements : public QObject
{
    Q_OBJECT
public:
    Abonements(QObject * parent, Database * db);

    Q_INVOKABLE void addAbonement(const int collectionId, const QString &title, const QString &feedUrl, const QString &iconUrl, const QStringList &recentEpisodeUrls);
    Q_INVOKABLE void removeAbonement(const int collectionId);
    Q_INVOKABLE bool isAbonement(const int collectionId);
    Q_INVOKABLE bool markHeard(const int collectionId, const QString & url, const bool heard = true);
    Q_INVOKABLE bool isHeard(const int collectionId, const QString &url);

    QList<Abonement> abonements();

signals:
    void updatedHeard(const QVariant & url, const QVariant & heard);

private slots:
    void readAbonements();
    void updateAbonement(const int collectionId);

private slots:
    void updateAbonements();

private:
    Database * database_;
    QHash<int, Abonement> abonements_;
};
