#include <QtGui/QGuiApplication>

#include <abonementlistmodel.h>
#include <abonements.h>
#include <cachingnetworkacessmangerfactory.h>
#include <database.h>
#include <downloadlistmodel.h>
#include <downloadmanager.h>
#include <settings.h>

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlEngine>
#include <QStandardPaths>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setApplicationName("QPod");
//    app.setApplicationVersion("1.0");

    QQmlApplicationEngine engine;
    Database database(engine.offlineStoragePath());
    if (!database.openDatabase()) {
        qDebug () << "Could not open/create database";
    }
    Settings settings;
    Abonements abos (0, &database);
    DownloadManager downloadManager;
    DownloadListModel downloadListModel(&downloadManager);
    AbonementListModel aboListModel(&abos);

    engine.rootContext()->setContextProperty("Abonements",        &abos);
    engine.rootContext()->setContextProperty("DownloadManager",   &downloadManager);
    engine.rootContext()->setContextProperty("downloadListModel", &downloadListModel);
    engine.rootContext()->setContextProperty("aboListModel",      &aboListModel);
    engine.rootContext()->setContextProperty("Settings",          &settings);
    engine.setNetworkAccessManagerFactory(new CachingNetworkAcessMangerFactory(QStandardPaths::writableLocation(QStandardPaths::CacheLocation)));
    engine.load(QUrl("qrc:/qml/main.qml"));

    return app.exec();
}
