QT += xmlpatterns sql core qml quick

TARGET = QPod
TEMPLATE = app

CONFIG += c++11

lessThan(QT_MAJOR_VERSION, 5) {
    error("requires Qt >= 5.5")
} else {
    lessThan(QT_MINOR_VERSION, 5) {
        error("requires Qt >= 5.5")
    }
}

# Default rules for deployment.
include(deployment.pri)

SOURCES += \
    abonementlistmodel.cpp \
    abonements.cpp \
    cachingnetworkacessmangerfactory.cpp \
    database.cpp \
    downloadlistmodel.cpp \
    downloadmanager.cpp \
    main.cpp \
    settings.cpp \

HEADERS += \
    abonement.h \
    abonementlistmodel.h \
    abonements.h \
    cachingnetworkacessmangerfactory.h \
    database.h \
    downloadlistmodel.h \
    downloadmanager.h \
    settings.h \

RESOURCES += \
    assets.qrc \
    qml.qrc \
