#include "downloadlistmodel.h"

#include <QDebug>

DownloadListModel::DownloadListModel(DownloadManager *manager, QObject *parent)
    :
      QAbstractListModel(parent),
      manager_(manager)
{
    connect(manager_, SIGNAL(progressUpdated(Download)),  this, SLOT(onProgressUpdated(Download)));
    connect(manager_, SIGNAL(downloadAdded(Download)),    this, SLOT(onDownloadAdded(Download)));
    connect(manager_, SIGNAL(downloadError(Download)),    this, SLOT(onDownloadError(Download)));
    connect(manager_, SIGNAL(downloadFinished(Download)), this, SLOT(onDownloadFinished(Download)));
}

int DownloadListModel::rowCount(const QModelIndex &/*parent*/) const
{
    return downloads_.count();
}

int DownloadListModel::columnCount(const QModelIndex & /*parent*/) const
{
    static int colCount = roleNames().size();
    return colCount;
}

QVariant DownloadListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()) return QVariant();

    const Download & download = downloads_[index.row()];
    if (role == BasenameRole)          return QUrl(download.url).fileName();
    else if (role == ProgressRole)     return download.percent;
    else if (role == ErrorMessageRole) return download.errorMessage;

    return QVariant();
}

void DownloadListModel::onDownloadAdded(const Download &download)
{
    if (download.url.isEmpty())        return;
    if (downloads_.contains(download)) return;

    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    downloads_.append(download);
    endInsertRows();

    qDebug() << "added download to row" << downloads_.size();
}

void DownloadListModel::onDownloadFinished(const Download &download)
{
    if (download.url.isEmpty())         return;
    if (!downloads_.contains(download)) return;

    const int row = downloads_.indexOf(download);
    beginRemoveRows(QModelIndex(), row, rowCount());
    downloads_.removeAt(row);
    endRemoveRows();
}

void DownloadListModel::onDownloadError(const Download & download)
{
    if (download.url.isEmpty())         return;
    if (!downloads_.contains(download)) return;

    const int row = downloads_.indexOf(download);
    downloads_.replace(row, download);
    emit dataChanged(createIndex(row, 0), createIndex(row, columnCount()), QVector<int>() << ProgressRole);
}

void DownloadListModel::onProgressUpdated(const Download &download)
{
    if (download.url.isEmpty())         return;
    if (!downloads_.contains(download)) return;

    const int row = downloads_.indexOf(download);
    downloads_.replace(row, download);

    qDebug() << downloads_.count() << " progress for row: " << row << "updated to" << download.percent;
    emit dataChanged(createIndex(row, 0), createIndex(row, columnCount()), QVector<int>() << ProgressRole);
}

QHash<int, QByteArray> DownloadListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[BasenameRole]     = "basename";
    roles[ProgressRole]     = "progress";
    roles[ErrorMessageRole] = "error";
    return roles;
}
