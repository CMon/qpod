#pragma once

#include <downloadmanager.h>

#include <QAbstractListModel>

class DownloadListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DownloadRoles {
        BasenameRole = Qt::UserRole + 1,
        ProgressRole,
        ErrorMessageRole
    };

    explicit DownloadListModel(DownloadManager * manager, QObject *parent = 0);

public:
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role) const;

public slots:
    void onDownloadAdded(const Download & download);
    void onDownloadFinished(const Download & download);
    void onDownloadError(const Download & download);
    void onProgressUpdated(const Download & download);

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    DownloadManager * manager_;

    Downloads downloads_;
};
