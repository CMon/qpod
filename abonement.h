#pragma once

#include <QDateTime>
#include <QMap>

class Abonement
{
public:
    Abonement() : collectionId(-1) {}
    Abonement(int collectionId, const QDateTime & lastChecked, const QString & title,
              const QString & feedUrl, const QString & iconUrl, const QMap<QString, bool> & recentEpisodeUrls)
        : collectionId(collectionId), lastChecked(lastChecked), title(title), feedUrl(feedUrl),
          iconUrl(iconUrl), recentEpisodeUrls(recentEpisodeUrls)
    {}

    bool operator<(const Abonement & rhs) const {
        if (title == rhs.title) return collectionId < rhs.collectionId;
        return title < rhs.title;
    }

    bool operator == (const Abonement & rhs) const {
        return
                collectionId      == rhs.collectionId &&
                lastChecked       == rhs.lastChecked &&
                title             == rhs.title &&
                feedUrl           == rhs.feedUrl &&
                iconUrl           == rhs.iconUrl &&
                lastTrack         == rhs.lastTrack &&
                recentEpisodeUrls == rhs.recentEpisodeUrls;
    }

    bool operator != (const Abonement & rhs) const {
        return !operator==(rhs);
    }


public:
    int       collectionId;
    QDateTime lastChecked;
    QString   title;
    QString   feedUrl;
    QString   iconUrl;
    QString   lastTrack;
    QMap<QString, bool> recentEpisodeUrls;
};
