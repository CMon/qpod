#include "abonementlistmodel.h"

#include <abonements.h>

#include <QtAlgorithms>

AbonementListModel::AbonementListModel(Abonements * abonements, QObject *parent)
    :
      QAbstractListModel(parent)
    , abonements_(abonements->abonements())
{
    qSort(abonements_);
}

int AbonementListModel::rowCount(const QModelIndex &) const
{
    return abonements_.size();
}

int AbonementListModel::columnCount(const QModelIndex &) const
{
    return roleNames().size();
}

QVariant AbonementListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()) return QVariant();

    switch(role) {
    case CollectionIdRole: return abonements_.at(index.row()).collectionId;
    case LastCheckedRole:  return abonements_.at(index.row()).lastChecked;
    case FeedUrlRole:      return abonements_.at(index.row()).feedUrl;
    case TitleRole:        return abonements_.at(index.row()).title;
    case IconUrlRole:      return abonements_.at(index.row()).iconUrl;
    }

    return QVariant();
}

QHash<int, QByteArray> AbonementListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[CollectionIdRole] = "collectionId";
    roles[LastCheckedRole]  = "lastChecked";
    roles[FeedUrlRole]      = "feedUrl";
    roles[TitleRole]        = "title";
    roles[IconUrlRole]      = "iconUrl";
    return roles;
}

