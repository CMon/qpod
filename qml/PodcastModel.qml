import QtQuick 2.0

Item {
    id: wrapper
    property variant model: listModel
    property string searchTerm : ""
    property bool loading: false
    readonly property string __itunesSearchBaseUrl: "https://itunes.apple.com/search?media=podcast&entity=podcast&attribute=titleTerm&country=DE&term=";

    function reload() {
        listModel.reload();
    }

    function search() {
        listModel.clear();
        wrapper.loading = true;
        var xhr = new XMLHttpRequest();
        var encoded = encodeURIComponent(searchTerm);

        xhr.open("GET", __itunesSearchBaseUrl+encoded, true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == xhr.DONE) {
                if (xhr.status == 200) {
                    loaded(JSON.parse(xhr.responseText))
                } else if (xhr.status == 0) {
                    console.log("Error: ", xhr.status, xhr.statusText);
                    // this typically happens if no internet connection is there at all
                    loadNoInternetMocObjects();
                }
            } else if (xhr.status != 200) {
                console.log("Error: ", xhr.status)
            }
            wrapper.loading = false;
        }
        xhr.send();
    }
//collectionId
//trackId
//artistName
//collectionName
//trackName
//collectionCensoredName
//trackCensoredName
//collectionViewUrl
//feedUrl
//trackViewUrl
//artworkUrl30
//artworkImage30
//artworkUrl60
//artworkUrl100
//releaseDate
//trackCount
//country
//primaryGenreName
//artworkUrl600
//genreIds
//genres

    function loaded(jsonObject) {
        for (var podcast in jsonObject.results) {
            listModel.append({
                                 "collectionId"    : jsonObject.results[podcast].collectionId,
                                 "collectionName"  : jsonObject.results[podcast].collectionName,
                                 "artworkUrl30"    : jsonObject.results[podcast].artworkUrl30,
                                 "artworkUrl60"    : jsonObject.results[podcast].artworkUrl60,
                                 "artworkUrl100"   : jsonObject.results[podcast].artworkUrl100,
                                 "artistName"      : jsonObject.results[podcast].artistName,
                                 "trackCount"      : jsonObject.results[podcast].trackCount,
                                 "feedUrl"         : jsonObject.results[podcast].feedUrl,
                                 "genre"           : jsonObject.results[podcast].genres[0]
                             });
        }
    }

    function loadNoInternetMocObjects() {
        listModel.append({
                             "collectionId"    : 1,
                             "collectionName"  : "Dummy Collection name 1",
                             "artworkUrl100"   : "assets/images/settings.png",
                             "artistName"      : "Dummy Artist 1",
                             "trackCount"      : 1,
                             "feedUrl"         : "mocFeedUrl",
                             "genre"           : "Dummy Genre 1"
                         });
        listModel.append({
                             "collectionId"    : 2,
                             "collectionName"  : "Dummy Collection name 2",
                             "artworkUrl100"   : "assets/images/lineedit.png",
                             "artistName"      : "Dummy Artist 2",
                             "trackCount"      : 2,
                             "feedUrl"         : "mocFeedUrl",
                             "genre"           : "Dummy Genre 2"
                         });
    }

    ListModel {
        id: listModel
    }
}
