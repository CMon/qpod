import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.2

Button {
    id: myButton

    property color baseColor: "grey"

    property color baseColorPressedFrom: "#ccc"
    property color baseColorPressedTo:   "#aaa"
    property color baseColorFrom:        "#eee"
    property color baseColorTo:          "#ccc"

    onBaseColorChanged: {
        baseColorPressedFrom = baseColor;
        baseColorPressedTo   = Qt.darker(baseColor, 1.5);
        baseColorFrom        = Qt.lighter(baseColor, 1.5);
        baseColorTo          = baseColor;
    }

    style: ButtonStyle {
        background: Rectangle {
            border.width: control.activeFocus ? 2 : 1
            border.color: "#000"
            radius: 4
            gradient: Gradient {
                GradientStop { position: 0 ; color: control.pressed ? baseColorPressedFrom : baseColorFrom }
                GradientStop { position: 1 ; color: control.pressed ? baseColorPressedTo   : baseColorTo }
            }
        }
    }
}
