import QtQuick 2.4
import QtQuick.Controls 1.4

FocusScope {
    id: root
    property alias title: settingTitleId.text
    property alias content: settingsComponent.content
    property alias type: settingsComponent.type

    height: appWindow.singleLineHeight

    Text {
        id: settingTitleId
        anchors.left: parent.left
        font.pixelSize: appWindow.fontPixelSize
    }

    Loader {
        id: settingsComponent
        anchors.left: settingTitleId.right
        anchors.top: settingTitleId.top
        anchors.right: parent.right
        anchors.leftMargin: 5
        height: parent.height

        property string content: "error"
        property int type: 1
        sourceComponent: {
            switch (type) {
            case 1: return inputDelegate;
            case 2: return spinDelegate;
            default:
                console.error("Programming error, undefined type("+type+") for setting");
            }
            return undefined;
        }
    }

    Component {
        id: inputDelegate
        TextField {
            text: content
            onTextChanged: settingsComponent.content = text
        }
    }

    Component {
        id: spinDelegate
        SpinBox {
            value: content
            onValueChanged: settingsComponent.content = value
        }
    }
}

