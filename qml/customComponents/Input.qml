import QtQuick 2.0

FocusScope {
    id: container
    BorderImage {
        border.left: 10
        border.top: 10
        border.bottom: 10
        border.right: 10
        source: "../assets/images/lineedit.png"
        anchors.fill: parent
    }
    signal accepted
    property alias text: input.text
    property alias item: input

    TextInput {
        id: input
        width: parent.width - 12
        anchors.centerIn: parent
        font.pixelSize: appWindow.fontPixelSize
        font.bold: true
        color: "#151515"
        selectionColor: "mediumseagreen"
        focus: true
        onAccepted: { container.accepted() }
        selectByMouse: true
        inputMethodHints: Qt.ImhNoPredictiveText
        verticalAlignment: Text.AlignVCenter
    }
}
