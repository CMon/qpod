import QtQuick 2.0

MyButton {
    height: parent.height
    opacity: 1
    width: height + height/2
    text: qsTr("Back")
    onClicked: if (!stackView.busy) stackView.pop();
}
