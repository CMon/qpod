import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.2

import "pages"

/// @todo:
/// - add a more button for podcasts that have more episodes than 50
/// - implement autoupdate + download of abos
/// - remove all listened episodes from device (setting)
/// - add sync of db through storing db in a specific dir
/// - add help screen which describes how to sync abos through db in dropbox/cloud folder
/// - add a number of unheard casts next to the podcast in the Search List/Abo Page
/// - update abonementlistmodel when adding removing abos from abonements

/// Bug:
/// * markHEard is not stored when closing app
/// * when download is heard is clicked the list with the tracks(icons) should be updated

ApplicationWindow {
    id: appWindow
    visible: true
    width: 360
    height: 480
    title: qsTr("QPod")
    color: "grey"

    readonly property double toolBarHeight:      8.0 * Screen.pixelDensity * Screen.devicePixelRatio
    readonly property double fontPixelSize:      3.5 * Screen.pixelDensity * Screen.devicePixelRatio
    readonly property double singleLineHeight:   fontPixelSize + 2 * Screen.pixelDensity * Screen.devicePixelRatio

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: PageFrontMenu { }
        focus: true
        Keys.onReleased: {
            if (event.key === Qt.Key_Back || event.key === Qt.Key_Escape) {
                if (stackView.depth > 1) {
                    stackView.pop()
                    event.accepted = true
                }
            }
        }
    }
}
