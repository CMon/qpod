import QtQuick 2.0

import "../customComponents"

BlankPage {
    id: pageFrontMenu
    x: 0
    y: 0
    width: parent.width
    height: parent.height
    title: qsTr("StartMenu")

    Column {
        width: parent.width
        anchors.verticalCenter: parent.verticalCenter
        spacing: 5

        MyButton {
            id: searchPageButton
            width: parent.width / 3
            height: appWindow.singleLineHeight
            text: qsTr("Search")
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: if (!stackView.busy) stackView.push(Qt.resolvedUrl("../pages/PageSearch.qml"));
        }

        MyButton {
            id: abosPageButton
            width: parent.width / 3
            height: appWindow.singleLineHeight
            text: qsTr("Abos")
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: if (!stackView.busy) stackView.push(Qt.resolvedUrl("../pages/PageAbos.qml"));
        }

        MyButton {
            id: unheardPageButton
            width: parent.width / 3
            height: appWindow.singleLineHeight
            text: qsTr("Unheard")
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: if (!stackView.busy) stackView.push(Qt.resolvedUrl("../pages/PageUnheard.qml"));
        }

        MyButton {
            id: downloadsPageButton
            width: parent.width / 3
            height: appWindow.singleLineHeight
            text: qsTr("Downloads")
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: if (!stackView.busy) stackView.push(Qt.resolvedUrl("../pages/PageDownloads.qml"));
        }

        MyButton {
            id: settingsPageButton
            width: parent.width / 3
            height: appWindow.singleLineHeight
            text: qsTr("Settings")
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: if (!stackView.busy) stackView.push(Qt.resolvedUrl("../pages/PageSettings.qml"));
        }

    }
}
