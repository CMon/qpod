import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import "../customComponents"

BlankPage {
    id: settingsView
    x: 0
    y: 0
    width: parent.width
    height: parent.height
    title: qsTr("Settings")

    Component.onCompleted: {
        downloadPathSetting.content    = Settings.downloadDirectory();
        updateIntervallSetting.content = Settings.updateAbonementsIntervallInDays();
    }

    ToolBar {
        id: settingsBar
        anchors.top: parent.top
        width: parent.width
        height: appWindow.toolBarHeight
        style: ToolBarStyle {
            background: Rectangle {
                color: "darkgrey"
            }
        }

        BackButton {
            anchors.left: parent.left
        }

        MyButton {
            id: saveButton
            height: parent.height
            anchors.right: parent.right
            opacity: 1
            text: qsTr("Save")
            onClicked: {
                if (downloadPathSetting.content != "")    Settings.setDownloadDirectory(downloadPathSetting.content);
                if (updateIntervallSetting.content != "") Settings.setUpdateAbonementsIntervallInDays(updateIntervallSetting.content);
            }
        }
    }

    Column {
        anchors.top: settingsBar.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        spacing: 5

        SettingsItem {
            id: downloadPathSetting
            title: qsTr("Download path:")
            width: parent.width
            type: 1
        }

        SettingsItem {
            id: updateIntervallSetting
            title: qsTr("Abo days to autoupdate:")
            width: parent.width
            type: 2
        }
    }

}
