import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import "../customComponents"

FocusScope {
    id: searchView

    ToolBar {
        id: searchToolBar
        width: parent.width
        height: parent.height
        anchors.centerIn: parent
        style: ToolBarStyle {
            background: Rectangle {
                color: "darkgrey"
            }
        }

        BackButton {
            id: backButton
        }

        Input {
            id: searchString
            height: searchButton.height
            width: parent.width - searchButton.getWidth() - backButton.width
            anchors.left: backButton.right
            anchors.leftMargin: 2
            KeyNavigation.backtab: searchButton
            KeyNavigation.tab: searchButton
            onAccepted: searchButton.doSearch()
            focus: true
        }

        MyButton {
            id: searchButton
            anchors.left: searchString.right
            anchors.leftMargin: 2
            height: parent.height
            opacity: 1
            text: qsTr("Search iTunes")
            KeyNavigation.tab: searchString.item
            Keys.onReturnPressed: searchButton.doSearch()
            Keys.onEnterPressed: searchButton.doSearch()
            Keys.onSelectPressed: searchButton.doSearch()
            Keys.onSpacePressed: searchButton.doSearch()
            onClicked: searchButton.doSearch()

            function doSearch() {
                if (searchView.state=="invalidinput")
                    return;
                podcastModel.searchTerm = searchString.text
                podcastModel.search()
            }

            function getWidth() {
                return width + anchors.leftMargin + anchors.rightMargin
            }
        }
    }
    states:
        State {
        name: "invalidinput"
        when: searchString.text == ""
        PropertyChanges { target: searchButton ; opacity: 0.6 ; }
    }
    transitions:
        Transition {
        NumberAnimation { target: searchButton; property: "opacity"; duration: 200 }
    }
}
