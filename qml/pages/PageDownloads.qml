import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import "../customComponents"
import "../delegates"
import "../"

BlankPage {
    id: downloadView
    x: 0
    y: 0
    width: parent.width
    height: parent.height
    title: qsTr("Downloads")

    ToolBar {
        id: toolbar
        anchors.top: parent.top
        width: parent.width
        height: appWindow.toolBarHeight
        style: ToolBarStyle {
            background: Rectangle {
                color: "darkgrey"
            }
        }

        BackButton {
            anchors.left: parent.left
        }
    }

    DownloadDelegate {
        id: downloadDelegate
    }

    ScrollView {
        width: parent.width
        height: stackView.height - toolbar.height
        anchors.top: toolbar.bottom

        ListView {
            id: downloadListView

            model: downloadListModel
            delegate:downloadDelegate
        }
    }
}
