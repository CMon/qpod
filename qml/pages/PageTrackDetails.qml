import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtMultimedia 5.4

import "../customComponents"

BlankPage {
    id: pageTrackDetails
    x: 0
    y: 0
    width: parent.width
    height: parent.height
    title: qsTr("Track details")

    property int collectionId: -1
    property string url: ""

    property bool isDownloaded: false
    property bool isHeard: false
    property string description: ""
    property string trackTitle: ""

    onUrlChanged: {
        isDownloaded = DownloadManager.isDownloaded(url);
        isHeard = Abonements.isHeard(collectionId, url);
    }

    onIsDownloadedChanged: {
        if (isDownloaded) {
            mediaPlayer.source = "file://" + DownloadManager.getPathToDownloadedUrl(url)
        } else {
            mediaPlayer.source = url
        }

        console.log("updated media source to: ", mediaPlayer.source);
    }

    ToolBar {
        id: trackDetailsBar
        width: parent.width
        height: appWindow.toolBarHeight
        anchors.top: parent.top
        style: ToolBarStyle {
            background: Rectangle {
                color: "darkgrey"
            }
        }

        BackButton {
            anchors.left: parent.left
        }

        MyButton {
            id: heardButton
            height: parent.height
            anchors.right: downloadButton.left
            text: qsTr("heard")
            baseColor: isHeard ? "green" : "red"
            onClicked: {
                isHeard = !isHeard;
                Abonements.markHeard(collectionId, isHeard);
            }
            enabled: isDownloaded
            opacity: 1.0
        }
        MyButton {
            id: downloadButton
            height: parent.height
            anchors.right: parent.right
            text: qsTr("Down")
            onClicked: {
                if (!isDownloaded) {
                    DownloadManager.addDownload(url);
                }
            }
            enabled: !isDownloaded
            opacity: isDownloaded ? 0.6 : 1.0
        }
    }

    MediaPlayer {
        id: mediaPlayer
        source: url
    }

    Rectangle {
        width: parent.width
        anchors.top: trackDetailsBar.bottom
        color: "lightgrey"
        anchors.bottom: parent.bottom
        Column {
            width: parent.width
            spacing: 10
            Text {
                id: titleId
                text: trackTitle
                wrapMode: Text.Wrap
                width: parent.width
                font.bold: true
            }
            Text {
                id: descriptionId
                text: description
                wrapMode: Text.Wrap
                width: parent.width
            }

            Image {
                id: player
                source: mediaPlayer.playbackState === MediaPlayer.PlayingState ? "../assets/images/pause.png"
                                                                               : "../assets/images/play.png"
                opacity: isDownloaded ? 1.0 : 0.1
                anchors.horizontalCenter: parent.horizontalCenter

                MouseArea {
                    id: playArea
                    anchors.fill: parent
                    onPressed: mediaPlayer.playbackState === MediaPlayer.PlayingState ? mediaPlayer.pause()
                                                                                      : mediaPlayer.play();
                }
            }

            Text {
                id: playbackSpeed
                text: qsTr("Playback speed (click me to change): ") + mediaPlayer.playbackRate

                MouseArea {
                    id: testArea
                    anchors.fill: parent
                    onPressed: mediaPlayer.playbackRate = Math.max((mediaPlayer.playbackRate + 0.1) % 2, 1)
                }
            }
        }
    }
}
