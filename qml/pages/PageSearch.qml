import QtQuick 2.0
import QtQuick.Controls 1.4

import "../"
import "../delegates"

BlankPage {
    id: pageSearch
    x: 0
    y: 0
    width: parent.width
    height: parent.height
    title: qsTr("Search")

    SearchView {
        id: searchView
        x: 0
        y: 0
        width: parent.width
        height: appWindow.toolBarHeight
        z: podcastList.z + 1;
    }

    PodcastModel {
        id: podcastModel
    }
    PodcastDelegate {
        id: podcastDelegate
    }

    ScrollView {
        width: parent.width
        height: stackView.height - searchView.height
        anchors.top: searchView.bottom

        ListView {
            id: podcastList

            model: podcastModel.model
            delegate: podcastDelegate
            cacheBuffer: 100
        }
    }
}
