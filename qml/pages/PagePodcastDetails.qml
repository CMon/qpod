import QtQuick 2.0
import QtQuick.XmlListModel 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

import "../customComponents"
import "../"
import "../delegates"

BlankPage {
    id: pagePodcastDetails
    x: 0
    y: 0
    width: parent.width
    height: parent.height
    title: qsTr("Podcast details")

    property int collectionId: -1
    property string feedUrl: ""
    property string artworkUrl30
    property string artworkUrl60
    property string artworkUrl100
    property string title
    property string description
    property string genre

    property bool isAbo: false

    onCollectionIdChanged: {
        isAbo = Abonements.isAbonement(collectionId);
    }

    ToolBar {
        id: detailsBar
        width: parent.width
        height: appWindow.toolBarHeight
        anchors.top: parent.top
        style: ToolBarStyle {
            background: Rectangle {
                color: "darkgrey"
            }
        }
        z: podcastTrackList.z + 1

        BackButton {
            anchors.left: parent.left
        }

        MyButton {
            id: aboButton
            height: parent.height
            anchors.right: parent.right
            text: isAbo ? qsTr("remove Abo") : qsTr("add Abo")
            onClicked: {
                if (isAbo) {
                    Abonements.removeAbonement(collectionId);
                } else {
                    Abonements.addAbonement(collectionId, title, feedUrl, image.source, xmlFeedListModel.getAllEpisodeUrls());
                }
                isAbo = !isAbo;
            }
            enabled: xmlFeedListModel.status == XmlListModel.Ready
            opacity: xmlFeedListModel.progress
        }
    }

    Rectangle {
        id: info
        width: parent.width
        anchors.top: detailsBar.bottom
        height: 100
        color: "lightgrey"
        z: podcastTrackList.z + 1

        Row {
            anchors.fill: parent
            spacing: 5
            opacity: 1

            LoadingImageItem {
                id: image
                width: parent.height
                height: parent.height
                source: height < 40 ? artworkUrl30
                                    : height < 80 ? artworkUrl60
                                                   : artworkUrl100
            }

            Text {
                width: parent.width
                id: podcastInfo
                text:
                '<html>'+ title + '<br>' +
                '<small>' + description + '</small>' + '<br>' +
                '<small>' + genre + '</small>' + '<br>' +
                '</html>'
                textFormat: Qt.RichText
                color: "black"
                styleColor: "black"
                wrapMode: Text.WordWrap
            }
        }
    }

    XmlListModel {
        id: xmlFeedListModel
        source: feedUrl
        query: "/rss/channel/item"

        namespaceDeclarations: "declare namespace itunes=\"http://www.itunes.com/dtds/podcast-1.0.dtd\";" +
                               "declare namespace media=\"http://search.yahoo.com/mrss/\";" +
                               "declare namespace feedburner=\"http://rssnamespace.org/feedburner/ext/1.0\";"

        XmlRole { name: "title";       query: "title/string()" }
        XmlRole { name: "description"; query: "description/string()" }
        XmlRole { name: "pubDate";     query: "pubDate/string()" }
        XmlRole { name: "link";        query: "enclosure/@url/string()" }
        XmlRole { name: "type";        query: "enclosure/@type/string()" }
        XmlRole { name: "duration";    query: "itunes:duration/string()" }

        function getAllEpisodeUrls () {
            var i = 0;
            var elem = null;
            var retval = [];
            while ((elem = xmlFeedListModel.get(i++)) != null) {
                retval.push(elem.link);
            }
            return retval;
        }
    }

    TrackDelegate {
        id: trackDelegate
    }

    Component.onCompleted: {
        // create moc elements
        for (var i = 0; i < 30; ++i) {
            mocModel.append({
                                "title"       : "Titel " + i,
                                "description" : "Description " + i,
                                "pubDate"     : "2013-12-27",
                                "link"        : "www.ccc.de",
                                "type"        : "audio",
                                "duration"    : "2:00:" + i
                            });
        }
    }

    ListModel {
        id: mocModel
    }

    ScrollView {
        width: pagePodcastDetails.width
        height: pagePodcastDetails.height - info.height - detailsBar.height
        anchors.top: info.bottom

        ListView {
            id: podcastTrackList

            model: feedUrl == "mocFeedUrl" ? mocModel : xmlFeedListModel
            delegate: trackDelegate
            cacheBuffer: 100
        }
    }
}
