import QtQuick 2.0

import "../"

Component {
    id: podcastDelegate

    Item {
        id: podcast
        width: podcastList.width
        height: if (txt.height > 60) { txt.height + 10 } else { 60 } //50+5+5

        // A simple rounded rectangle for the background
        Rectangle {
            x: 1
            y: 1
            width: parent.width - x*2; height: parent.height - y*2
            color: "black"
            border.color: "grey"
            radius: 5
            opacity: index % 2 ? 0.2 : 0.3
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (!stackView.busy) {
                    stackView.push(
                                {
                                    item: Qt.resolvedUrl("../pages/PagePodcastDetails.qml"),
                                    properties: {
                                        collectionId  : collectionId,
                                        feedUrl       : feedUrl,
                                        artworkUrl30  : artworkUrl30,
                                        artworkUrl60  : artworkUrl60,
                                        artworkUrl100 : artworkUrl100,
                                        title         : artistName,
                                        description   : collectionName,
                                        genre         : genre
                                    }
                                });
                }
            }
        }

        Row {
            x: 10
            height: 48
            width: parent.width
            spacing: 5
            anchors.verticalCenter: parent.verticalCenter

            LoadingImageItem {
                id: image
                width: parent.height
                height: parent.height
                source: height < 40 ? artworkUrl30
                                    : height < 80 ? artworkUrl60
                                                   : artworkUrl100
            }

            Text {
                id: txt
                x: 2
                text: '<html><b>' +
                      artistName + '</b><br>' +
                      collectionName + '<br>' +
                      trackCount + ' Episodes<br>' +
                      '</html>'
                textFormat: Qt.RichText
                color: "lightgrey"
                wrapMode: Text.WordWrap
            }
        }
    }
}
