import QtQuick 2.0
import "../pages"

Component {
    id: trackDelegate

    Item {
        id: track
        width: parent.width
        height: trackText.height + 10

        property string trackUrl: link

        // A simple rounded rectangle for the background
        Rectangle {
            id: backgroundRect
            x: 1
            y: 1
            width: parent.width - x*2
            height: parent.height - y*2
            color: "black"
            border.color: "grey"
            radius: 5
            opacity: index % 2 ? 0.2 : 0.3
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if (!stackView.busy) {
                    stackView.push(
                                {
                                    item: Qt.resolvedUrl("../pages/PageTrackDetails.qml"),
                                    properties: {
                                        url          : track.trackUrl,
                                        collectionId : collectionId,
                                        description  : description,
                                        trackTitle   : title
                                    }
                                });
                }
            }
        }

        Row {
            id: topLayout
            x: 10
            width: backgroundRect.width - x
            spacing: 5
            anchors.verticalCenter: parent.verticalCenter

            Column {
                id: trackText
                width: parent.width - iconColumn.width - topLayout.spacing
                spacing: 2

                Row {
                    width: parent.width
                    spacing: 3
                    Text {
                        id: trackTitle
                        color: "lightgrey"
                        text: title
                        wrapMode: Text.WordWrap
                    }
                    Text {
                        id: trackDuration
                        color: "lightgrey"
                        text: '(' + duration + ')'
                        wrapMode: Text.NoWrap
                        font.pointSize: trackTitle.font.pointSize - 2
                    }
                }

                Text {
                    id: trackDescription
                    color: "lightgrey"
                    text: description
                    wrapMode: Text.WordWrap
                    width: parent.width
                    font.pointSize: trackTitle.font.pointSize - 2
                }

                Text {
                    id: trackDate
                    color: "lightgrey"
                    text: pubDate
                    wrapMode: Text.WordWrap
                    width: parent.width
                    font.pointSize: trackTitle.font.pointSize - 2
                }
            }

            Column {
                id: iconColumn
                width: 16
                Connections {
                    ignoreUnknownSignals: true
                    target: DownloadManager
                    onDownloadFinishedWithOk: {
                        if (url != track.trackUrl) return;
                        isDownloaded.opacity = 1;
                    }
                }
                Connections {
                    ignoreUnknownSignals: true
                    target: Abonements
                    onUpdatedHeard: {
                        console.log(args);
                        console.log(url);// why is the url a string???
                        console.log(heard);
                        if (url != track.trackUrl) return;

                        isHeard.opacity = heard ? 1 : 0.1
                    }
                }
                Image {
                    id: isDownloaded
                    source: "../assets/images/castDownloaded.png"
                    width: parent.width
                    opacity: DownloadManager.isDownloaded(track.trackUrl) ? 1 : 0.1
                }
                Image {
                    id: isHeard
                    source: "../assets/images/castListened.png"
                    width: parent.width
                    opacity: Abonements.isHeard(collectionId, track.trackUrl) ? 1 : 0.1
                }
            }
        }
    }
}
