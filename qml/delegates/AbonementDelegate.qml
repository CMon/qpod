import QtQuick 2.0
import QtQuick.Controls 1.3

import "../"

Component {

    Item {
        id: download
        width: parent.width
        height: appWindow.toolBarHeight

        // A simple rounded rectangle for the background
        Rectangle {
            id: backgroundRect
            x: 1
            y: 1
            width: parent.width - x*2; height: parent.height - y*2
            color: "black"
            border.color: "grey"
            radius: 5
            opacity: index % 2 ? 0.2 : 0.3
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log("should we do something on click?")
            }
        }

        Row {
            anchors.fill: backgroundRect
            anchors.margins: 2
            spacing: 5
            anchors.verticalCenter: backgroundRect.verticalCenter

            LoadingImageItem {
                id: image
                height: parent.height - parent.anchors.margins * 2
                width: height
                source: iconUrl
                anchors.verticalCenter: parent.verticalCenter
            }

            Text {
                text: collectionId
                color: "#cccccc"
                wrapMode: Text.NoWrap
            }
            Text {
                text: lastChecked
                color: "#cccccc"
                wrapMode: Text.NoWrap
            }
            Text {
                text: feedUrl
                color: "#cccccc"
                wrapMode: Text.NoWrap
            }
            Text {
                text: title
                color: "#cccccc"
                wrapMode: Text.NoWrap
            }
        }
    }
}
