import QtQuick 2.0
import QtQuick.Controls 1.3

Component {
    id: downloadDelegate

    Item {
        id: download
        width: parent.width
        height: if (txt.height > 60) { txt.height + 10 } else { 60 } //50+5+5

        // A simple rounded rectangle for the background
        Rectangle {
            x: 1
            y: 1
            width: parent.width - x*2; height: parent.height - y*2
            color: "black"
            border.color: "grey"
            radius: 5
            opacity: index % 2 ? 0.2 : 0.3
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log("should we do something on click?")
            }
        }

        Column {
            x: 10
            height: 48
            width: parent.width
            spacing: 5
            anchors.verticalCenter: parent.verticalCenter

            Text {
                id: txt
                x: 2
                text: basename
                color: "#cccccc"
                style: Text.Raised
                styleColor: "black"
                wrapMode: Text.WordWrap
            }

            ProgressBar {
                id: progressBar
                x: parent.width / 10
                width: parent.width - x * 2
                minimumValue: 0
                maximumValue: 100
                indeterminate: progress == -1

                value: progress
            }
        }
    }
}
