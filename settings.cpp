#include "settings.h"

#include <QStandardPaths>
#include <QSettings>

QVariant SettingsBase::readEntry(const QString &key, const QVariant defaultValue)
{
    QSettings settings("CMon", "QPod");
    return settings.value(key, defaultValue);
}

void SettingsBase::writeEntry(const QString &key, const QVariant &value)
{
    QSettings settings("CMon", "QPod");
    settings.setValue(key, value);
}


Settings::Settings(QObject *parent)
    :
      QObject(parent)
{
}

QString Settings::downloadDirectory()
{
    return readEntry("downloadDirectory", QStandardPaths::standardLocations(QStandardPaths::MusicLocation).first()).toString();
}

void Settings::setDownloadDirectory(const QString &directory)
{
    writeEntry("downloadDirectory", directory);
}

int Settings::updateAbonementsIntervallInDays()
{
    return readEntry("updateAbonementsIntervallInDays", 1).toInt();
}

void Settings::setUpdateAbonementsIntervallInDays(const int days)
{
    writeEntry("updateAbonementsIntervallInDays", days);
}

