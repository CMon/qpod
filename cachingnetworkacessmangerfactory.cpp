#include "cachingnetworkacessmangerfactory.h"

#include <QDir>
#include <QNetworkAccessManager>
#include <QNetworkDiskCache>
#include <QGuiApplication>

CachingNetworkAcessMangerFactory::CachingNetworkAcessMangerFactory(const QString &baseCachePath)
{
    cachePath_ = baseCachePath;
    qDebug() << "Caching files to: " << cachePath_;
}

QNetworkAccessManager* CachingNetworkAcessMangerFactory::create(QObject* parent)
{
    QNetworkAccessManager* manager = new QNetworkAccessManager(parent);
    QNetworkDiskCache* cache = new QNetworkDiskCache(manager);
    cache->setCacheDirectory(cachePath_);
    cache->setMaximumCacheSize(5 * 1024 * 1024); // ~ 5MB
    manager->setCache(cache);
    return manager;
}
