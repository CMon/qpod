#pragma once

#include <QSqlDatabase>

class QString;
class QStringList;
class Abonement;

class Database
{
public:
    Database(const QString &offlineStoragePath);
    ~Database();

    bool openDatabase();

    bool addAbonement(const Abonement & abo, const bool defaultHeard = true);
    bool markEpisodeHeard(const int collectionId, const QString & episodeUrl, const bool heard = true);
    bool removeAbonement(const int collectionId);
    QList<Abonement> getAllAbonements();

private:
    bool tablesExists();
    bool createDatabase();

private:
    QString databasePath_;
    QSqlDatabase db_;
};
