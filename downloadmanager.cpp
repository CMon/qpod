#include "downloadmanager.h"

#include <settings.h>

#include <QDebug>

static int calcPercent(qint64 bytesReceived, qint64 bytesTotal)
{
    return (double) bytesReceived / (double) bytesTotal * 100.0;
}

static int calcPercent(QPair<qint64, qint64> bytes)
{
    return calcPercent(bytes.first, bytes.second);
}

DownloadManager::DownloadManager(QObject *parent)
    :
      QObject(parent),
      manager_(this)
{
    connect(&manager_, SIGNAL(finished(QNetworkReply*)), this, SLOT(downloadFinished(QNetworkReply*)));

    if (Settings::downloadDirectory().isEmpty()) Settings::setDownloadDirectory(QDir::tempPath());
    qDebug() << "Going to download to: " << Settings::downloadDirectory();
}

void DownloadManager::addDownload(const QUrl &url)
{
    if (!createDownloadDirectory()) return;
    if (url.isEmpty()) return;
    if (isAlreadyDownloaded(url)) return;
    if (isAlreadyDownloading(url)) return;

    QNetworkRequest request;
    request.setUrl(url);
    QNetworkReply * reply = manager_.get(request);
    connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(progressUpdated(qint64,qint64)));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(onError(QNetworkReply::NetworkError)));

    replys_.append(reply);
    progressCache_[url] = qMakePair(0, 0);

    qDebug() << "download of" << url << "started";
    emit downloadAdded(Download(url, 0));
}

bool DownloadManager::isDownloaded(const QString &url) const
{
    if (url.isEmpty()) return false;

    return isAlreadyDownloaded(url) || isAlreadyDownloading(url);
}

QString DownloadManager::getPathToDownloadedUrl(const QString &url) const
{
    if (!isAlreadyDownloaded(url)) return QString();
    return createStorePath(url);
}

QList<Download> DownloadManager::getCurrentDownloads() const
{
    QList<Download> retval;
    foreach (const QNetworkReply * reply, replys_) {
        int percent = 0;
        if (progressCache_.contains(reply->url())) percent = calcPercent(progressCache_[reply->url()]);
        retval << Download(reply->url(), percent);
    }
    return retval;
}

void DownloadManager::progressUpdated(qint64 bytesReceived, qint64 bytesTotal)
{
    const QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
    if (!reply) return;

    const int oldPercent = calcPercent(progressCache_[reply->url()]);
    progressCache_[reply->url()] = qMakePair(bytesReceived, bytesTotal);

    const int percent = calcPercent(bytesReceived, bytesTotal);

    if (percent != oldPercent) emit progressUpdated(Download(reply->url(), percent));
}

void DownloadManager::downloadFinished(QNetworkReply *reply)
{
    reply->deleteLater();
    replys_.removeOne(reply);
    const int percent = calcPercent(progressCache_.value(reply->url()));
    progressCache_.remove(reply->url());

    const QString filename = createStorePath(reply->url());

    QFile storeTo(filename);

    if (!storeTo.open(QIODevice::WriteOnly)) {
        const QString errorMsg = tr("Could not open file (%1) for storing, error: %2").arg(filename).arg(storeTo.errorString());
        qDebug() << errorMsg;
        emit downloadError(Download(reply->url(), percent, errorMsg));
        return;
    }

    /// @todo: catch error
    storeTo.write(reply->readAll());

    storeTo.flush();
    storeTo.close();

    emit downloadFinishedWithOk(QVariant::fromValue(reply->url()));
    emit downloadFinished(Download(reply->url(), 100));
}

void DownloadManager::onError(QNetworkReply::NetworkError)
{
    QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
    if (!reply) return;

    qDebug () << "Error: " << reply->errorString();

    reply->deleteLater();
    const int percent = calcPercent(progressCache_.value(reply->url()));
    progressCache_.remove(reply->url());
    emit downloadError(Download(reply->url(), percent, reply->errorString()));
}

bool DownloadManager::isAlreadyDownloaded(const QUrl & url) const
{
    const QString filename = createStorePath(url);
    const bool fileExists = QFile::exists(filename);
    if (fileExists) {
        qDebug () << "url already downloaded. Stored to: " << filename;
    }

    return fileExists;
}

bool DownloadManager::isAlreadyDownloading(const QUrl & url) const
{
    foreach (const QNetworkReply * reply, replys_) {
        if (reply->url() == url) return true;
    }

    return false;
}

QString DownloadManager::createStorePath(const QUrl &url) const
{
    return Settings::downloadDirectory() + QDir::separator() + url.fileName();
}

bool DownloadManager::createDownloadDirectory()
{
    QDir dlDir;

    if (!dlDir.exists(Settings::downloadDirectory())) {
        if (dlDir.mkpath(Settings::downloadDirectory())) {
            qDebug () << "Created download directory: " << Settings::downloadDirectory();
        } else {
            qDebug () << "Creation of  download directory: " << Settings::downloadDirectory() << "failed";
            return false;
        }
    }

    return true;
}
