#include "abonements.h"

#include <database.h>
#include <settings.h>

#include <QTimer>
#include <QDateTime>

#include <QDebug>

Abonements::Abonements(QObject *parent, Database *db)
    :
      QObject(parent),
      database_(db)
{
    readAbonements();
    updateAbonements();
}

void Abonements::addAbonement(const int collectionId, const QString & title, const QString & feedUrl,
                              const QString & iconUrl, const QStringList & recentEpisodeUrls)
{
    if (collectionId < 0 || abonements_.contains(collectionId)) return;

    QMap<QString, bool> recentEpisodeUrlsHeard;
    foreach (const QString url, recentEpisodeUrls) {
        recentEpisodeUrlsHeard[url] = false;
    }
    const Abonement abo = Abonement(collectionId, QDateTime::currentDateTime(), title, feedUrl, iconUrl,
                                    recentEpisodeUrlsHeard);

    abonements_[collectionId] = abo;
    database_->addAbonement(abo, true);

    QTimer::singleShot(0, this, SLOT(updateAbonements()));
}

void Abonements::removeAbonement(const int collectionId)
{
    if (collectionId < 0 || !abonements_.contains(collectionId)) return;

    abonements_.remove(collectionId);
    database_->removeAbonement(collectionId);
}

bool Abonements::isAbonement(const int collectionId)
{
    return abonements_.contains(collectionId);
}

bool Abonements::markHeard(const int collectionId, const QString & url, const bool heard)
{
    abonements_[collectionId].recentEpisodeUrls[url] = heard;

    emit updatedHeard(QVariant::fromValue(url), QVariant::fromValue(heard));
    return database_->markEpisodeHeard(collectionId, url, heard);
}

bool Abonements::isHeard(const int collectionId, const QString & url)
{
    if (!abonements_.contains(collectionId) || !abonements_[collectionId].recentEpisodeUrls.contains(url)) return false;

    return abonements_[collectionId].recentEpisodeUrls[url];
}

QList<Abonement> Abonements::abonements()
{
    return abonements_.values();
}

void Abonements::readAbonements()
{
    abonements_.clear();

    const QList<Abonement> abos = database_->getAllAbonements();
    foreach (const Abonement & abo, abos) {
        abonements_[abo.collectionId] = abo;
    }
}

void Abonements::updateAbonement(const int collectionId)
{
    if (collectionId < 0) return;

    const Abonement abo = abonements_.value(collectionId);
    qDebug () << "TODO: update Abo from: " << abo.feedUrl;
}

void Abonements::updateAbonements()
{
    const QDateTime checkIfOlderThanThis = QDateTime::currentDateTime().addDays(- Settings::updateAbonementsIntervallInDays());
    foreach(const Abonement & abo, abonements_.values()) {
        if (abo.lastChecked.isNull() || abo.lastChecked <= checkIfOlderThanThis) {
            updateAbonement(abo.collectionId);
        }
    }
}
