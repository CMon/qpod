#include "database.h"

#include <abonement.h>

#include <QDir>
#include <QSqlError>
#include <QSqlQuery>

#include <QDebug>

#define execQuery(query) \
if (!query.exec()) { \
    qWarning() << __LINE__ << query.lastError(); \
    return false; \
}

Database::Database(const QString & offlineStoragePath)
    :
      databasePath_(offlineStoragePath + QDir::separator() + "qpod")
{
    QDir dummy;
    dummy.mkpath(offlineStoragePath);
    qDebug () << "Database Path:" << databasePath_;
}

Database::~Database()
{
    if (!db_.isOpen()) db_.close();
}

bool Database::openDatabase()
{
    if (db_.isOpen()) return true;

    db_ = QSqlDatabase::addDatabase("QSQLITE");
    db_.setHostName("localhost");
    db_.setDatabaseName(databasePath_);

    bool ok = db_.open();
    if (!ok) {
        qWarning() << db_.lastError().number() << db_.lastError().text();
    } else {
        createDatabase();
    }

    return ok;
}

bool Database::addAbonement(const Abonement & abo, const bool defaultHeard)
{
    QSqlQuery query(db_);
    query.prepare("INSERT INTO abonements "
                    "(collectionId, lastChecked, title, feedUrl, iconUrl) "
                  "VALUES "
                    "(:collectionId, :lastChecked, :title, :feedUrl, :iconUrl)");
    query.bindValue(":collectionId", abo.collectionId);
    query.bindValue(":lastChecked",  abo.lastChecked);
    query.bindValue(":title",        abo.title);
    query.bindValue(":feedUrl",      abo.feedUrl);
    query.bindValue(":iconUrl",      abo.iconUrl);

    execQuery(query)

    if (abo.recentEpisodeUrls.isEmpty()) return true;

    query.prepare("INSERT INTO aboEpisodes "
                    "(collectionId, episodeUrl, heard) "
                  "VALUES "
                    "(:collectionId, :episodeUrl, :heard)");
    foreach(const QString & episodeUrl, abo.recentEpisodeUrls.keys()) {
        query.bindValue(":collectionId", abo.collectionId);
        query.bindValue(":episodeUrl",   episodeUrl);
        query.bindValue(":heard",        defaultHeard);
        execQuery(query)
    }

    return true;
}

bool Database::markEpisodeHeard(const int collectionId, const QString &episodeUrl, const bool heard)
{
    QSqlQuery query(db_);
    query.prepare("UPDATE "
                    "aboEpisodes "
                  "SET "
                    "heard=:heard "
                  "WHERE "
                    "collectionId=:collectionId AND "
                    "episodeUrl=:episodeUrl");
    query.bindValue(":heard", heard);
    query.bindValue(":collectionId", collectionId);
    query.bindValue(":episodeUrl",   episodeUrl);
    execQuery(query)
    return true;
}

bool Database::removeAbonement(const int collectionId)
{
    QSqlQuery query(db_);
    query.prepare("DELETE FROM "
                    "abonements "
                  "WHERE "
                    "collectionId=:collectionId");

    query.bindValue(":collectionId", collectionId);
    execQuery(query)

    query.prepare("DELETE FROM "
                    "aboEpisodes "
                  "WHERE "
                    "collectionId=:collectionId");
    query.bindValue(":collectionId", collectionId);
    execQuery(query)
    return true;
}

QList<Abonement> Database::getAllAbonements()
{
    QSqlQuery query(db_);
    query.prepare("SELECT "
                    "collectionId, lastChecked, title, feedUrl, iconUrl "
                  "FROM "
                    "abonements "
                  "ORDER BY "
                    "collectionId");
    if (!query.exec()) {
        qWarning() << query.lastError();
        return QList<Abonement>();
    }

    QSqlQuery innerQuery(db_);
    innerQuery.prepare("SELECT "
                         "episodeUrl, heard "
                       "FROM "
                         "aboEpisodes "
                       "WHERE "
                         "collectionId=:collectionId");

    QList<Abonement> retval;
    while(query.next()) {
        const int collectionId      = query.value(0).toInt();
        const QDateTime lastChecked = query.value(1).toDateTime();
        const QString title         = query.value(2).toString();
        const QString feedUrl       = query.value(3).toString();
        const QString iconUrl       = query.value(4).toString();

        innerQuery.bindValue(":collectionId", collectionId);
        if(!innerQuery.exec()) {
            qWarning() << query.lastError();
            continue;
        }
        QMap<QString, bool> urls;
        while(innerQuery.next()) {
            urls[innerQuery.value(0).toString()] = innerQuery.value(1).toBool();
        }
        retval << Abonement(collectionId, lastChecked, title, feedUrl, iconUrl, urls);
    }
    return retval;
}

bool Database::tablesExists()
{
    QStringList tablesToTest;
    tablesToTest << "abonements"
                 << "aboEpisodes";
    QSqlQuery test(db_);
    foreach (const QString & table, tablesToTest) {
        test.prepare("Select COUNT(*) FROM " + table);
        if (!test.exec()) {
            qDebug() << "Table" << table << "does not exist";
            return false;
        }
    }

    return true;
}

bool Database::createDatabase()
{
    if (tablesExists()) return true;

    bool retval = true;
    QSqlQuery query(db_);
    retval = retval && query.exec("CREATE TABLE abonements("
                                  "collectionId INTEGER PRIMARY KEY,"
                                  "lastChecked DATETIME,"
                                  "title VARCHAR(255),"
                                  "feedUrl VARCHAR(255),"
                                  "iconUrl VARCHAR(255)"
                                  ")");

    retval = retval && query.exec("CREATE TABLE aboEpisodes("
                                  "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                                  "collectionId INTEGER,"
                                  "episodeUrl VARCHAR(255),"
                                  "heard INTEGER)");

    if (!retval) qWarning() << "Could not create database";
    qDebug() << "db created";

    return retval;
}
