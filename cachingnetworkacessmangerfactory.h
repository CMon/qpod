#pragma once

#include <QQmlNetworkAccessManagerFactory>

class CachingNetworkAcessMangerFactory : public QQmlNetworkAccessManagerFactory
{
public:
    CachingNetworkAcessMangerFactory(const QString & baseCachePath);
    QNetworkAccessManager* create(QObject* parent);

private:
    QString cachePath_;
};
