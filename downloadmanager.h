#pragma once

#include <QObject>
#include <QNetworkAccessManager>
#include <QDir>
#include <QUrl>
#include <QPair>
#include <QNetworkReply>

struct Download
{
    Download(const QUrl & url, const int percent, const QString & errorMsg = QString())
        : url(url), percent(percent), errorMessage(errorMsg) {}

    QUrl url;
    int  percent;
    QString errorMessage;
};

class Downloads : public QList<Download>
{
public:
    bool contains(const Download & download) const {
        for(int i = 0; i < count(); ++i) {
            if (at(i).url == download.url) {
                return true;
            }
        }
        return false;
    }

    int indexOf(const Download & download, int from = 0) const {
        int row = -1;
        for(int i = from; i < count(); ++i) {
            if (at(i).url == download.url) {
                row = i;
            }
        }
        return row;
    }
};

class DownloadManager : public QObject
{
    Q_OBJECT
public:
    DownloadManager(QObject * parent = 0);

    Q_INVOKABLE void addDownload(const QUrl & url);
    Q_INVOKABLE bool isDownloaded(const QString & url) const;
    Q_INVOKABLE QString getPathToDownloadedUrl(const QString & url) const;
    QList<Download> getCurrentDownloads() const;

signals:
    void downloadAdded(const Download & download);
    void progressUpdated(const Download & download);
    void downloadFinished(const Download & download);
    void downloadError(const Download & download);
    void downloadFinishedWithOk(const QVariant & url);

private slots:
    void progressUpdated(qint64 bytesReceived, qint64 bytesTotal);
    void downloadFinished(QNetworkReply * reply);
    void onError(QNetworkReply::NetworkError error);

private:
    bool isAlreadyDownloaded(const QUrl &url) const;
    bool isAlreadyDownloading(const QUrl & url) const;
    QString createStorePath(const QUrl &url) const;

    bool createDownloadDirectory();

private:
    QNetworkAccessManager manager_;
    QList<QNetworkReply*> replys_;

    QHash<QUrl, QPair<qint64, qint64> > progressCache_;
};

