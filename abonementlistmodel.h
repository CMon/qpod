#pragma once

#include <abonement.h>

#include <QAbstractListModel>

class Abonements;

class AbonementListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum AbonementListRoles {
        CollectionIdRole = Qt::UserRole + 1,
        LastCheckedRole,
        FeedUrlRole,
        TitleRole,
        IconUrlRole
    };

    AbonementListModel(Abonements * abonements, QObject *parent = 0);

public:
    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role) const;

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    QList<Abonement> abonements_;
};
